// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package net.minecraft.src;

import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;


// Referenced classes of package net.minecraft.src:
//            RenderLiving, EntityWolf, ModelBase, EntityLiving, 
//            Entity

public class CJB_DogRender extends RenderLiving
{
	
	private CJB_DogModel modelArmor;
	
	private static final String armorFilenamePrefix[] = {
        "cloth", "chain", "iron", "diamond", "gold"
    };

    public CJB_DogRender(ModelBase modelbase, float f)
    {
        super(modelbase, f);
        modelArmor = new CJB_DogModel(0.5F);
    }
    
    protected int setArmorModel(CJB_DogEntity entitydog, int i)
    {
    	if (i > 0)
    		return -1;
    	
        ItemStack itemstack = entitydog.inventory.armorItemInSlot(0);
        if(itemstack != null)
        {
            Item item = itemstack.getItem();
            if(item instanceof ItemArmor)
            {
                ItemArmor itemarmor = (ItemArmor)item;
                loadTexture("/cjb/mobs/dog/dogarmor_" + armorFilenamePrefix[itemarmor.renderIndex] + ".png");
	           	CJB_DogModel modeldog = modelArmor;
	           	modeldog.onGround = 0;
	          	modeldog.dogBody.showModel = true;
	          	modeldog.dogMane.showModel = true;
	          	modeldog.dogHeadMain.showModel = true;
	          	modeldog.dogCollar.showModel = false;
	          	modeldog.dogLeftEar.showModel = false;
	          	modeldog.dogLeftEarTop.showModel = false;
	          	modeldog.dogLeg1.showModel = false;
	          	modeldog.dogLeg2.showModel = false;
	          	modeldog.dogLeg3.showModel = false;
	          	modeldog.dogLeg4.showModel = false;
	          	modeldog.dogNose.showModel = false;
	          	modeldog.dogRightEar.showModel = false;
	          	modeldog.dogRightEarTop.showModel = false;
	          	modeldog.dogSnout.showModel = false;
	          	modeldog.dogTail.showModel = false;
	          	setRenderPassModel(modeldog);
	          	return !itemstack.isItemEnchanted() ? 1 : 15;
            }
        }
        return -1;
    }

    public void renderDog(CJB_DogEntity entitydog, double d, double d1, double d2, 
            float f, float f1)
    {
    	float f7 = entitydog.field_705_Q + (entitydog.field_704_R - entitydog.field_705_Q) * f1;
        float f8 = entitydog.field_703_S - entitydog.field_704_R * (1.0F - f1);
        modelArmor.setLivingAnimations(entitydog, f8, f7, f1);
        super.doRenderLiving(entitydog, d, d1, d2, f, f1);
    }

    protected float getTailRotation(CJB_DogEntity entitydog)
    {
        return entitydog.setTailRotation();
    }

    protected float handleRotationFloat(EntityLiving entityliving, float f)
    {
        return getTailRotation((CJB_DogEntity)entityliving);
    }

    public void doRenderLiving(EntityLiving entityliving, double d, double d1, double d2, 
            float f, float f1)
    {
        renderDog((CJB_DogEntity)entityliving, d, d1, d2, f, f1);
    }

    public void doRender(Entity entity, double d, double d1, double d2, 
            float f, float f1)
    {
        renderDog((CJB_DogEntity)entity, d, d1, d2, f, f1);
    }
    
    protected void passSpecialRender(EntityLiving entityliving, double d, double d1, double d2)
    {
        renderName((CJB_DogEntity)entityliving, d, d1, d2);
    }
    
    protected void preRenderCallback(EntityLiving par1EntityLiving, float par2)
    {
    }
    
    protected int shouldRenderPass(EntityLiving entityliving, int i, float f)
    {
        return setArmorModel((CJB_DogEntity)entityliving, i);
    }
    
    protected void renderName(CJB_DogEntity entitydog, double d, double d1, double d2)
    {
        if(entitydog.isTamed() && Minecraft.isGuiEnabled() && entitydog != renderManager.livingPlayer)
        {
            float f = 1.0F;
            float f1 = 0.01666667F * f;
            float f2 = entitydog.getDistanceToEntity(renderManager.livingPlayer);
            float f3 = 5F;
            if(f2 < f3)
            {
                String s = entitydog.health +  "/" + entitydog.maxHealth;
                FontRenderer fontrenderer = getFontRendererFromRenderManager();
             	GL11.glPushMatrix();
                GL11.glTranslatef((float)d + 0.0F, (float)d1 + 1.2F, (float)d2);
                GL11.glNormal3f(0.0F, 1.0F, 0.0F);
            	GL11.glRotatef(-renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
            	GL11.glRotatef(renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
            	GL11.glScalef(-f1, -f1, f1);
              	GL11.glDisable(2896 /*GL_LIGHTING*/);
              	GL11.glEnable(3553 /*GL_TEXTURE_2D*/);
              	fontrenderer.drawString(s, -fontrenderer.getStringWidth(s) / 2, 0, 0x20ffffff);
              	GL11.glEnable(2896 /*GL_LIGHTING*/);
              	GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
              	GL11.glPopMatrix();
            }
        }
    }
}
