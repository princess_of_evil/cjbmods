package net.minecraft.src;
import java.util.List;
import org.lwjgl.input.Keyboard;
import net.minecraft.client.Minecraft;

public class mod_cjb_cheats extends BaseMod {
	
	private boolean keystop;
	Minecraft m;
	
	private static boolean flypressed;
	public static boolean cheatsloaded;
	
	public mod_cjb_cheats()
	{
		ModLoader.setInGameHook(this, true, false);
		CJB.modcheats = true;
		
		PlayerAPI.register("CJB Mods", CJB_Player.class);
	}
	
	
	public static void loadCheats()
	{
		cheatsloaded = true;
		CJB.infinitehealth = CJB_Settings.getBooleanW("cheat.infinitehealth", false);
		CJB.infinitebreath = CJB_Settings.getBooleanW("cheat.infinitebreath", false);
		CJB.unbreakableitems = CJB_Settings.getBooleanW("cheat.unbreakableitems", false);
		CJB.onehitblock = CJB_Settings.getBooleanW("cheat.onehitblock", false);
		CJB.onehitkill = CJB_Settings.getBooleanW("cheat.onehitkill", false);
		CJB.walkoversoil = CJB_Settings.getBooleanW("cheat.walkoversoil", false);
		CJB.instantfurnace = CJB_Settings.getBooleanW("cheat.instantfurnace", false);
		CJB.infinitearrows = CJB_Settings.getBooleanW("cheat.infinitearrows", false);
		CJB.unbreakablearmor = CJB_Settings.getBooleanW("cheat.unbreakablearmor", false);
		CJB.alwaysday = CJB_Settings.getBooleanW("cheat.alwaysday", false);
		CJB.alwaysnight = CJB_Settings.getBooleanW("cheat.alwaysnight", false);
		CJB.nofalldamage = CJB_Settings.getBooleanW("cheat.nofalldamage", false);
		CJB.cartcontrol = CJB_Settings.getBooleanW("cheat.cartcontrol", false);
		CJB.extendedreach = CJB_Settings.getBooleanW("cheat.extendedreach", false);
		CJB.fastgrowth = CJB_Settings.getBooleanW("cheat.fastgrowth", false);
		CJB.nopickaxe = CJB_Settings.getBooleanW("cheat.nopickaxe", false);
		CJB.pickupallarrows = CJB_Settings.getBooleanW("cheat.pickupallarrows", false);
		CJB.attractiveitems = CJB_Settings.getBooleanW("cheat.attractiveitems", false);
		CJB.mobspawner = CJB_Settings.getBooleanW("cheat.mobspawner", false);
		CJB.infiniteitems = CJB_Settings.getBooleanW("cheat.infiniteitems", false);
		CJB.rain = CJB_Settings.getBooleanW("cheat.rain", false);
		CJB.thunder = CJB_Settings.getBooleanW("cheat.thunder", false);
		CJB.infinitexp = CJB_Settings.getBooleanW("cheat.infinitexp", false);
		CJB.nofirespread = CJB_Settings.getBooleanW("cheat.nofirespread", false);
		CJB.pickupdistance = CJB_Settings.getIntegerW("cheat.pickupdistance", 1);
		CJB.disablehunger = CJB_Settings.getBooleanW("cheat.disablehunger", false);
		CJB.infinitelava = CJB_Settings.getBooleanW("cheat.infinitelava", false);
		
		CJB.mouseControl = CJB_Settings.getBoolean("cheat.flymousecontrol", true);
		CJB.flyspeed = CJB_Settings.getInteger("cheat.flyspeed", 3);
		CJB.runspeed = CJB_Settings.getInteger("cheat.runspeed", 3);
		CJB.speedMode = CJB_Settings.getBoolean("cheat.speedmode", false);
		CJB.autoJump = CJB_Settings.getBoolean("cheat.autojump", false);
		CJB.alwaysspeed = CJB_Settings.getBoolean("cheat.alwaysspeed", false);
		CJB.noclip = CJB_Settings.getBoolean("cheat.noclip", false);
		
		CJB.blackfog = CJB_Settings.getBoolean("cheat.blackfog", false);
	}
	
	public boolean onTickInGame(float f, Minecraft mc)
	{		
		m = mc;
		World world = mc.theWorld;
		EntityPlayer plr = mc.thePlayer;
		
		if (plr == null || world != mc.theWorld) {
			cheatsloaded = false;
			CJB.flying = false;
			return true;
		}
		
		plr.noClip = CJB.noclip && CJB.flying;
		
		if (!cheatsloaded)
			loadCheats();

		if (mc.currentScreen == null)
		{
			if (Keyboard.getEventKeyState())
			{				
				if (Keyboard.isKeyDown(CJB.KeyFly) && !flypressed)
				{
					flypressed = true;
					CJB.flying = !CJB.flying;
					
					if (CJB.pmnoclip && CJB.noclip && mc.isMultiplayerWorld()) {
						mc.thePlayer.sendChatMessage("/noclip " + (CJB.flying ? "enabled" : "disabled"));
					}
				}
				if (CJB.speedMode && Keyboard.isKeyDown(CJB.KeyFlySpeed) && !flypressed)
				{
					flypressed = true;
					CJB.toggleSpeed = !CJB.toggleSpeed;
				}
				if (!mc.isMultiplayerWorld() && Keyboard.isKeyDown(CJB.KeyGameMode) && !flypressed)
				{
					flypressed = true;
					if (mc.playerController instanceof PlayerControllerSP)
					{
						mc.playerController = new PlayerControllerCreative(mc);
						PlayerControllerCreative.enableAbilities(plr);
					} else {
						mc.playerController = new PlayerControllerSP(mc);
						PlayerControllerCreative.disableAbilities(plr);
					}
				}
			}
			if (!Keyboard.getEventKeyState()) {
				flypressed = false;
			}
		}
		
		if (!CJB.speedMode)
			CJB.toggleSpeed = false;
		
		if (!mc.playerController.isInCreativeMode())
			plr.capabilities.isCreativeMode = CJB.infiniteitems;
		
		CJB.guiopen = mc.currentScreen != null;
		
		if (!CJB.pmfly)
			CJB.flying = false;
		
		if(mc.isMultiplayerWorld())
			return true;
		
		if (CJB.disablehunger && !(plr.foodStats instanceof CJB_FoodStats))
			plr.foodStats = new CJB_FoodStats();
		
		if (!CJB.disablehunger && plr.foodStats instanceof CJB_FoodStats)
			plr.foodStats = new FoodStats();

		if (!(mc.playerController instanceof CJB_PlayerControllerSP) && mc.playerController instanceof PlayerControllerSP)
			mc.playerController = new CJB_PlayerControllerSP(mc);
		
		if (!(mc.playerController instanceof CJB_PlayerControllerTest) && mc.playerController instanceof PlayerControllerCreative)
			mc.playerController = new CJB_PlayerControllerTest(mc);
		
		if (!CJB.guiopen && CJB.cartcontrol)
		{
			updateRiding(plr, plr.ridingEntity);
		}
		
		if (CJB.walkoversoil && !(Block.blocksList[60] instanceof CJB_BlockFarmland)) {
			Block.blocksList[60] = null;
			Block.blocksList[60] = new CJB_BlockFarmland(60).setHardness(0.6F).setStepSound(new StepSound("gravel", 1.0F, 1.0F)).setBlockName("farmland");
		}
		if ((!CJB.walkoversoil || mc.isMultiplayerWorld()) && Block.blocksList[60] instanceof CJB_BlockFarmland)
		{
			Block.blocksList[60] = null;
			Block.blocksList[60] = new BlockFarmland(60).setHardness(0.6F).setStepSound(new StepSound("gravel", 1.0F, 1.0F)).setBlockName("farmland");
		}
		
		if (CJB.nofirespread && !(Block.blocksList[60] instanceof CJB_BlockFire)) {
			Block.blocksList[51] = null;
			Block.blocksList[51] = new CJB_BlockFire(51, 31).setHardness(0.0F).setLightValue(1.0F).setStepSound(Block.soundWoodFootstep).setBlockName("fire").disableStats();
		}
		if ((!CJB.nofirespread || mc.isMultiplayerWorld()) && Block.blocksList[60] instanceof BlockFire)
		{
			Block.blocksList[51] = null;
			Block.blocksList[51] = Block.fire;
		}
		if (CJB.infinitelava && !(Block.blocksList[10] instanceof CJB_BlockLava) && !mc.isMultiplayerWorld()) {
			Block.blocksList[10] = null;
			Block.blocksList[10] = (new CJB_BlockLava(10, Material.lava)).setHardness(0.0F).setLightValue(1.0F).setLightOpacity(255).setBlockName("lava").disableStats().setRequiresSelfNotify();
		} else if (!CJB.infinitelava || ((Block.blocksList[10] instanceof CJB_BlockLava) && mc.isMultiplayerWorld())) {
			Block.blocksList[10] = null;
			Block.blocksList[10] = (new BlockFlowing(10, Material.lava)).setHardness(0.0F).setLightValue(1.0F).setLightOpacity(255).setBlockName("lava").disableStats().setRequiresSelfNotify();
		}
		
		
		
		if (!mc.isGamePaused)
		{
			List list = world.getEntitiesWithinAABB(net.minecraft.src.EntityItem.class, AxisAlignedBB.getBoundingBoxFromPool(plr.posX, plr.posY, plr.posZ, plr.posX + 1.0D, plr.posY + 1.0D, plr.posZ + 1.0D).expand(60D, 60D, 60D));
			if (!list.isEmpty()) 
			{
				for (int i = 0 ; i < list.size(); i++)
				{
					EntityItem item = (EntityItem) list.get(i);
					ItemStack itemstack = item.item;

					if (plr.isDead || plr.getHealth() <= 0 || !canStoreItemStack(itemstack, plr) || item.isDead && (CJB.attractiveitems && CJB.pickupdistance > 1))
						continue;
	
					double d = 8D;
					double d1 = (plr.posX - item.posX);
		            double d2 = ((plr.posY + (double)plr.getEyeHeight()) - item.posY);
		            double d3 = (plr.posZ - item.posZ);
		            double d6 = Math.sqrt(d1 * d1 + d2 * d2 + d3 * d3);
		            
		            if (item.delayBeforeCanPickup <= 0 && CJB.attractiveitems) {
			            d1 /= d;
			            d2 /= d;
			            d3 /= d;
			            
			            double d4 = Math.sqrt(d1 * d1 + d2 * d2 + d3 * d3);
			            double d5 = 1.2D - d4;
			            if(d5 > 0.0D)
			            {
			                d5 *= d5;
			                item.motionX += (d1 / d4) * d5 * 0.10000000000000001D;
			                item.motionY += (d2 / d4) * d5 * 0.10000000000000001D;
			                item.motionZ += (d3 / d4) * d5 * 0.10000000000000001D;
			            }
		            }
		            if (!Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.keyCode) && CJB.pickupdistance > 1 && d6 < CJB.pickupdistance)
		            {
		            	int j = itemstack.stackSize;
		            	
			            if (j > 0 && plr.inventory.addItemStackToInventory(itemstack))
			            {
			                if (itemstack.itemID == Block.wood.blockID)
			                {
			                    plr.triggerAchievement(AchievementList.mineWood);
			                }
			                if (itemstack.itemID == Item.leather.shiftedIndex)
			                {
			                    plr.triggerAchievement(AchievementList.killCow);
			                }
			                if (itemstack.itemID == Item.diamond.shiftedIndex)
			                {
			                    plr.triggerAchievement(AchievementList.diamonds);
			                }
			                if (itemstack.itemID == Item.blazeRod.shiftedIndex)
			                {
			                    plr.triggerAchievement(AchievementList.blazeRod);
			                }
			                ModLoader.onItemPickup(plr, itemstack);
			                world.playSoundAtEntity(item, "random.pop", 0.2F, ((world.rand.nextFloat() - world.rand.nextFloat()) * 0.7F + 1.0F) * 2.0F);
			                plr.onItemPickup(item, j);
			                if (itemstack.stackSize <= 0)
			                {
			                    item.setDead();
			                }
			            }
		            }
				}
				
			}
		}
		if (CJB.fastgrowth && !(Block.blocksList[59] instanceof CJB_BlockCrops)) {
			Block.blocksList[59] = null;
			Block.blocksList[59] = (new CJB_BlockCrops(59, 88)).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setBlockName("crops").disableStats().setRequiresSelfNotify();
			Block.blocksList[83] = null;
			Block.blocksList[83] = (new CJB_BlockReed(83, 73)).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setBlockName("reeds").disableStats();
			Block.blocksList[6] = null;
			Block.blocksList[6] = (new CJB_BlockSapling(6, 15)).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setBlockName("sapling").setRequiresSelfNotify();
			Block.blocksList[81] = null;
			Block.blocksList[81] = (new CJB_BlockCactus(81, 70)).setHardness(0.4F).setStepSound(Block.soundClothFootstep).setBlockName("cactus");
			Block.blocksList[104] = null;
			Block.blocksList[104] = (new CJB_BlockStem(104, Block.pumpkin)).setHardness(0.0F).setStepSound(Block.soundWoodFootstep).setBlockName("pumpkinStem").setRequiresSelfNotify();
			Block.blocksList[105] = null;
			Block.blocksList[105] = (new CJB_BlockStem(105, Block.melon)).setHardness(0.0F).setStepSound(Block.soundWoodFootstep).setBlockName("pumpkinStem").setRequiresSelfNotify();
			Block.blocksList[39] = null;
			Block.blocksList[39] = (BlockFlower)(new CJB_BlockMushroom(39, 29)).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setLightValue(0.125F).setBlockName("mushroom");
			Block.blocksList[40] = null;
			Block.blocksList[40] = (BlockFlower)(new CJB_BlockMushroom(40, 28)).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setBlockName("mushroom");
			Block.blocksList[106] = null;
			Block.blocksList[106] = (new CJB_BlockVine(106)).setHardness(0.2F).setStepSound(Block.soundGrassFootstep).setBlockName("vine").setRequiresSelfNotify();
			
			Item.itemsList[256+95] = null;
			Item.dyePowder = (new CJB_ItemDye(95)).setIconCoord(14, 4).setItemName("dyePowder");
		}
		else if (!CJB.fastgrowth && (Block.blocksList[59] instanceof CJB_BlockCrops)) {
			Block.blocksList[59] = null;
			Block.blocksList[59] = (new BlockCrops(59, 88)).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setBlockName("crops").disableStats().setRequiresSelfNotify();
			Block.blocksList[83] = null;
			Block.blocksList[83] = (new BlockReed(83, 73)).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setBlockName("reeds").disableStats();
			Block.blocksList[6] = null;
			Block.blocksList[6] = (new BlockSapling(6, 15)).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setBlockName("sapling").setRequiresSelfNotify();
			Block.blocksList[81] = null;
			Block.blocksList[81] = (new BlockCactus(81, 70)).setHardness(0.4F).setStepSound(Block.soundClothFootstep).setBlockName("cactus");
			Block.blocksList[104] = null;
			Block.blocksList[104] = (new BlockStem(104, Block.pumpkin)).setHardness(0.0F).setStepSound(Block.soundWoodFootstep).setBlockName("pumpkinStem").setRequiresSelfNotify();
			Block.blocksList[105] = null;
			Block.blocksList[105] = (new BlockStem(105, Block.melon)).setHardness(0.0F).setStepSound(Block.soundWoodFootstep).setBlockName("pumpkinStem").setRequiresSelfNotify();
			Block.blocksList[39] = null;
			Block.blocksList[39] = (BlockFlower)(new BlockMushroom(39, 29)).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setLightValue(0.125F).setBlockName("mushroom");
			Block.blocksList[40] = null;
			Block.blocksList[40] = (BlockFlower)(new BlockMushroom(40, 28)).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setBlockName("mushroom");
			Block.blocksList[106] = null;
			Block.blocksList[106] = (new BlockVine(106)).setHardness(0.2F).setStepSound(Block.soundGrassFootstep).setBlockName("vine").setRequiresSelfNotify();
			
			Item.itemsList[256+95] = null;
			Item.dyePowder = (new ItemDye(95)).setIconCoord(14, 4).setItemName("dyePowder");
		}
		
		for (int i = 0 ; i < world.loadedEntityList.size() ; i++)
		{
			Entity ent = (Entity)world.loadedEntityList.get(i);
			
			if (CJB.pickupallarrows && ent instanceof EntityArrow && ((EntityArrow)ent).arrowShake == 0)
			{
				((EntityArrow)ent).doesArrowBelongToPlayer = true;
			}
			
			if (CJB.onehitkill && ent instanceof EntityArrow && ((EntityArrow)ent).shootingEntity instanceof EntityPlayer)
			{
				((EntityArrow)ent).setDamage(99999D);
			}              
		}
		
		if (CJB.instantfurnace) {
			for (int i = 0 ; i < world.loadedTileEntityList.size() ; i++)
			{
				TileEntity tileentity = (TileEntity)world.loadedTileEntityList.get(i);
				if (tileentity != null && tileentity instanceof TileEntityFurnace){
					TileEntityFurnace tile = (TileEntityFurnace) tileentity;
					tile.furnaceBurnTime = 300;
					
					if(tile.furnaceCookTime < 199)
						tile.furnaceCookTime = 199;
				}
			}
		}
		
		long time = mc.theWorld.getWorldTime() % 24000;
		
		if (CJB.alwaysday && time > 12000)
		{
			mc.theWorld.setWorldTime(mc.theWorld.getWorldTime() + 24000 - time);
		}
		
		if (CJB.alwaysnight && (time < 14000 || time > 22000))
		{
			mc.theWorld.setWorldTime(mc.theWorld.getWorldTime() + 15000 - time);
		}		
		
		if (CJB.rain && world.worldInfo.isRaining())
		{
			world.worldInfo.setRaining(false);
			world.worldInfo.setThundering(false);
		}
		
		if (CJB.thunder && world.worldInfo.isThundering())
		{
			world.worldInfo.setThundering(false);
		}

		return true;
	}
	
	private void updateRiding(EntityPlayer plr, Entity entity)
	{
		
		if (entity == null || !(entity instanceof EntityMinecart))
			return;
		
		EntityMinecart ent = (EntityMinecart) entity;
		
		keystop = Keyboard.isKeyDown(Keyboard.KEY_SPACE);
		double px = plr.motionX, pz = plr.motionZ;
		double mx = ent.motionX + px * 2f, mz = ent.motionZ + pz * 2f;
		double speed = Math.sqrt(mx*mx*+mz*mz), rate;
		
		if (speed > 1F)
		{
			rate = 1f / speed;
			mx *= rate; mz *= rate;
		}
		
		if (!CJB.guiopen && keystop){ mx = mz = 0.0D; }
		
		ent.motionX = mx;
		ent.motionZ = mz;
	}
	
	public static boolean canStoreItemStack(ItemStack itemstack, EntityPlayer plr)
    {
        for(int i = 0; i < plr.inventory.mainInventory.length; i++)
        {
        	if (plr.inventory.mainInventory[i] == null)
        		return true;
        	
            if(plr.inventory.mainInventory[i] != null && plr.inventory.mainInventory[i].itemID == itemstack.itemID && plr.inventory.mainInventory[i].isStackable() && plr.inventory.mainInventory[i].stackSize < plr.inventory.mainInventory[i].getMaxStackSize() && plr.inventory.mainInventory[i].stackSize < plr.inventory.getInventoryStackLimit() && (!plr.inventory.mainInventory[i].getHasSubtypes() || plr.inventory.mainInventory[i].getItemDamage() == itemstack.getItemDamage()))
            {
                return true;
            }
        }

        return false;
    }

	public String getVersion() {
		return CJB.VERSION;
	}

	public void load() {
	}  
	
	public static void dropBlockAsItem(World world, int i, int j, int k, ItemStack itemstack)
    {
        if(world.isRemote)
        {
            return;
        } else
        {
            float f = 0.7F;
            double d = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
            double d1 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
            double d2 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
            EntityItem entityitem = new EntityItem(world, (double)i + d, (double)j + d1, (double)k + d2, itemstack);
            entityitem.delayBeforeCanPickup = 10;
            world.spawnEntityInWorld(entityitem);
            return;
        }
    }
}