package net.minecraft.src;

public class CJB_DogEntity extends EntityTameable
{
	
	public CJB_DogInventory inventory;
    private boolean looksWithInterest;
    private float field_25048_b;
    private float field_25054_c;
    public boolean isDogShaking;
    private boolean isDogBusyShaking;
    private float timeDogIsShaking;
    private float prevTimeDogIsShaking;
    public int collarColor;
    public int maxHealth;
    
    public static final float CollarColorTable[][] = {
    	{
            1.0F, 1.0F, 1.0F
        }, {
            0.95F, 0.7F, 0.2F
        }, {
            0.9F, 0.5F, 0.85F
        }, {
            0.6F, 0.7F, 0.95F
        }, {
            0.9F, 0.9F, 0.2F
        }, {
            0.5F, 0.8F, 0.1F
        }, {
            0.95F, 0.7F, 0.8F
        }, {
            0.3F, 0.3F, 0.3F
        }, {
            0.6F, 0.6F, 0.6F
        }, {
            0.3F, 0.6F, 0.7F
        }, {
            0.7F, 0.4F, 0.9F
        }, {
            0.2F, 0.4F, 0.8F
        }, {
            0.5F, 0.4F, 0.3F
        }, {
            0.4F, 0.5F, 0.2F
        }, {
            0.8F, 0.3F, 0.3F
        }, {
            0.1F, 0.1F, 0.1F
        }
    };

    public CJB_DogEntity(World world)
    {
        super(world);
        inventory = new CJB_DogInventory(this);
        looksWithInterest = false;
        texture = "/cjb/mobs/dog/dog.png";
        setSize(0.8F, 0.8F);
        moveSpeed = 0.3F;
        collarColor = world.rand.nextInt(15);
        maxHealth = 20;
        health = getMaxHealth();
        getNavigator().setAvoidsWater(true);
        tasks.addTask(1, new EntityAISwimming(this));
        tasks.addTask(2, aiSit);
        tasks.addTask(3, new EntityAILeapAtTarget(this, 0.4F));
        tasks.addTask(4, new EntityAIAttackOnCollide(this, moveSpeed, true));
        tasks.addTask(5, new EntityAIFollowOwner(this, moveSpeed, 10F, 2.0F));
        tasks.addTask(6, new EntityAIMate(this, moveSpeed));
        tasks.addTask(7, new EntityAIWander(this, moveSpeed));
        tasks.addTask(8, new EntityAIWatchClosest(this, net.minecraft.src.EntityPlayer.class, 8F));
        tasks.addTask(9, new EntityAILookIdle(this));
        targetTasks.addTask(1, new EntityAIOwnerHurtByTarget(this));
        targetTasks.addTask(2, new EntityAIOwnerHurtTarget(this));
        targetTasks.addTask(3, new EntityAIHurtByTarget(this, true));
        targetTasks.addTask(4, new EntityAINearestAttackableTarget(this, net.minecraft.src.EntityMob.class, 16F, 0, false, true));
    }
    
    /**
     * Returns true if the newer Entity AI code should be run
     */
    public boolean isAIEnabled()
    {
        return true;
    }

    protected void entityInit()
    {
        super.entityInit();
        dataWatcher.addObject(18, new Integer(getHealth()));
    }
    
    /**
     * Sets the active target the Task system uses for tracking
     */
    public void setAttackTarget(EntityLiving par1EntityLiving)
    {
        super.setAttackTarget(par1EntityLiving);

        if (par1EntityLiving instanceof EntityPlayer)
        {
            setAngry(true);
        }
    }

    /**
     * main AI tick function, replaces updateEntityActionState
     */
    protected void updateAITick()
    {
        dataWatcher.updateObject(18, Integer.valueOf(getHealth()));
    }

    protected boolean canTriggerWalking()
    {
        return false;
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
        nbttagcompound.setTag("Inventory", inventory.writeToNBT(new NBTTagList()));
        nbttagcompound.setInteger("CollarColor", collarColor);
        nbttagcompound.setBoolean("Angry", isAngry());
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
        NBTTagList nbttaglist = nbttagcompound.getTagList("Inventory");
        inventory.readFromNBT(nbttaglist);
        setAngry(nbttagcompound.getBoolean("Angry"));
        collarColor = nbttagcompound.getInteger("CollarColor");
    }

    protected boolean canDespawn()
    {
        return isAngry();
    }

    protected String getLivingSound()
    {
        if(isAngry())
        {
            return "mob.wolf.growl";
        }
        if(rand.nextInt(3) == 0)
        {
            if(isTamed() && dataWatcher.getWatchableObjectInt(18) < 10)
            {
                return "mob.wolf.whine";
            } else
            {
                return "mob.wolf.panting";
            }
        } else
        {
            return "mob.wolf.bark";
        }
    }

    protected String getHurtSound()
    {
        return "mob.wolf.hurt";
    }

    protected String getDeathSound()
    {
        return "mob.wolf.death";
    }

    protected float getSoundVolume()
    {
        return 0.4F;
    }
    
    public void onDeath(DamageSource damagesource)
    {
    	inventory.dropAllItems();
        super.onDeath(damagesource);
    }

    public void onUpdate()
    {
        super.onUpdate();
        field_25054_c = field_25048_b;
        if(looksWithInterest)
        {
            field_25048_b = field_25048_b + (1.0F - field_25048_b) * 0.4F;
        } else
        {
            field_25048_b = field_25048_b + (0.0F - field_25048_b) * 0.4F;
        }
        if(looksWithInterest)
        {
            numTicksToChaseTarget = 10;
        }
        if(isWet())
        {
            isDogShaking = true;
            isDogBusyShaking = false;
            timeDogIsShaking = 0.0F;
            prevTimeDogIsShaking = 0.0F;
        } else
        if(isDogShaking && isDogBusyShaking)
        {
            if(timeDogIsShaking == 0.0F)
            {
                worldObj.playSoundAtEntity(this, "mob.wolf.shake", getSoundVolume(), (rand.nextFloat() - rand.nextFloat()) * 0.2F + 1.0F);
            }
            prevTimeDogIsShaking = timeDogIsShaking;
            timeDogIsShaking += 0.05F;
            if(prevTimeDogIsShaking >= 2.0F)
            {
                isDogShaking = false;
                isDogBusyShaking = false;
                prevTimeDogIsShaking = 0.0F;
                timeDogIsShaking = 0.0F;
            }
            if(timeDogIsShaking > 0.4F)
            {
                float f = (float)boundingBox.minY;
                int i = (int)(MathHelper.sin((timeDogIsShaking - 0.4F) * 3.141593F) * 7F);
                for(int j = 0; j < i; j++)
                {
                    float f1 = (rand.nextFloat() * 2.0F - 1.0F) * width * 0.5F;
                    float f2 = (rand.nextFloat() * 2.0F - 1.0F) * width * 0.5F;
                    worldObj.spawnParticle("splash", posX + (double)f1, f + 0.8F, posZ + (double)f2, motionX, motionY, motionZ);
                }

            }
        }
        /*if(health > 0 && isTamed() && !isSitting() && !isAngry && trackingItem)
        {
            List list = worldObj.getEntitiesWithinAABB(net.minecraft.src.EntityItem.class, boundingBox.expand(2.0D, 2.0D, 2.0D));
            if(list != null)
            {
                for(int i = 0; i < list.size(); i++)
                {
                    Entity entity = (Entity)list.get(i);
                    if(!entity.isDead)
                    {
                    	onCollideWithItem((EntityItem) entity);
                    	trackingItem = false;
                    }
                }
            }
        }*/
        
        maxHealth = 20 + inventory.getTotalArmorValue();
        
        if(health > maxHealth)
        	health = maxHealth;
    }
    
    public void heal(int i)
    {
        if(health <= 0)
        {
            return;
        }
        health += i;
        if(health > maxHealth)
        {
            health = maxHealth;
        }
        heartsLife = heartsHalvesLife / 2;
    }
    
    public void onCollideWithItem(EntityItem entityitem)
    {
        ItemStack item = entityitem.item;
        if(entityitem.delayBeforeCanPickup == 0 && inventory.addItemStackToInventory(item))
        {
            worldObj.playSoundAtEntity(entityitem, "random.pop", 0.2F, ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.0F) * 2.0F);
            if(item.stackSize <= 0)
            {
                entityitem.setDead();
                //trackingItem = false;
            }
        }
    }

    public float getShadingWhileShaking(float f)
    {
        return 0.75F + ((prevTimeDogIsShaking + (timeDogIsShaking - prevTimeDogIsShaking) * f) / 2.0F) * 0.25F;
    }

    public float getShakeAngle(float f, float f1)
    {
        float f2 = (prevTimeDogIsShaking + (timeDogIsShaking - prevTimeDogIsShaking) * f + f1) / 1.8F;
        if(f2 < 0.0F)
        {
            f2 = 0.0F;
        } else
        if(f2 > 1.0F)
        {
            f2 = 1.0F;
        }
        return MathHelper.sin(f2 * 3.141593F) * MathHelper.sin(f2 * 3.141593F * 11F) * 0.15F * 3.141593F;
    }

    public float getInterestedAngle(float f)
    {
        return (field_25054_c + (field_25048_b - field_25054_c) * f) * 0.15F * 3.141593F;
    }

    public float getEyeHeight()
    {
        return height * 0.8F;
    }

    public int getVerticalFaceSpeed()
    {
        if(isSitting())
        {
            return 20;
        } else
        {
            return super.getVerticalFaceSpeed();
        }
    }

    protected boolean isMovementCeased()
    {
        return isSitting() || isDogBusyShaking;
    }

    public boolean attackEntityFrom(DamageSource par1DamageSource, int par2)
    {
    	Entity entity = par1DamageSource.getEntity();
        aiSit.func_48407_a(false);

        if (entity != null && !(entity instanceof EntityPlayer) && !(entity instanceof EntityArrow))
        {
            par2 = (par2 + 1) / 2;
        }

        return super.attackEntityFrom(par1DamageSource, par2);
    }

    public boolean attackEntityAsMob(Entity par1Entity)
    {
        byte byte0 = ((byte)(isTamed() ? 4 : 2));
        return par1Entity.attackEntityFrom(DamageSource.causeMobDamage(this), byte0);
    }
    
    /**
     * Checks if the parameter is an wheat item.
     */
    public boolean isWheat(ItemStack par1ItemStack)
    {
        if (par1ItemStack == null)
        {
            return false;
        }

        if (!(Item.itemsList[par1ItemStack.itemID] instanceof ItemFood))
        {
            return false;
        }
        else
        {
            return ((ItemFood)Item.itemsList[par1ItemStack.itemID]).isWolfsFavoriteMeat();
        }
    }

    public boolean interact(EntityPlayer entityplayer)
    {
        ItemStack itemstack = entityplayer.inventory.getCurrentItem();
        if(!isTamed())
        {
            if(itemstack != null && itemstack.itemID == mod_cjb_mobs.dogcollar.shiftedIndex && !isAngry())
            {
                itemstack.stackSize--;
                if(itemstack.stackSize <= 0)
                {
                    entityplayer.inventory.setInventorySlotContents(entityplayer.inventory.currentItem, null);
                }
                if(!worldObj.isRemote)
                {
                    setTamed(true);
                    setPathToEntity(null);
                    setAttackTarget(null);
                    aiSit.func_48407_a(true);
                    setEntityHealth(20);
                    setOwner(entityplayer.username);
                    func_48142_a(true);
                    worldObj.setEntityState(this, (byte)7);
                }
                return true;
            }
        } else
        {
        	if(itemstack != null && itemstack.itemID == Item.bone.shiftedIndex)
        	{
        		if (isSitting())
        			ModLoader.openGUI(entityplayer, new CJB_DogGuiInventory(entityplayer.inventory, inventory, this));
        		
        		return true;
        	}
            if(itemstack != null && (Item.itemsList[itemstack.itemID] instanceof ItemFood))
            {
                ItemFood itemfood = (ItemFood)Item.itemsList[itemstack.itemID];
                if(itemfood.isWolfsFavoriteMeat())
                {
                	if (getHealth() < maxHealth) {
	                    itemstack.stackSize--;
	                    if(itemstack.stackSize <= 0)
	                    {
	                        entityplayer.inventory.setInventorySlotContents(entityplayer.inventory.currentItem, null);
	                    }
	                    heal(((ItemFood)Item.porkRaw).getHealAmount()*2);
	                    return true;
                	}
                    return true;
                }
            }
            if(itemstack != null && (Item.itemsList[itemstack.itemID] instanceof ItemDye))
            {
            	collarColor = 15-itemstack.getItemDamageForDisplay();
            	if (--itemstack.stackSize <= 0)
            		entityplayer.inventory.setInventorySlotContents(entityplayer.inventory.currentItem, null);
            	
            	return true;
            }

            aiSit.func_48407_a(!isSitting());
            setPathToEntity(null);
            return true;
              
        }
        return false;
    }

    void showHeartsOrSmokeFX(boolean flag)
    {
        String s = "heart";
        if(!flag)
        {
            s = "smoke";
        }
        for(int i = 0; i < 7; i++)
        {
            double d = rand.nextGaussian() * 0.02D;
            double d1 = rand.nextGaussian() * 0.02D;
            double d2 = rand.nextGaussian() * 0.02D;
            worldObj.spawnParticle(s, (posX + (double)(rand.nextFloat() * width * 2.0F)) - (double)width, posY + 0.5D + (double)(rand.nextFloat() * height), (posZ + (double)(rand.nextFloat() * width * 2.0F)) - (double)width, d, d1, d2);
        }

    }

    public void handleHealthUpdate(byte byte0)
    {
        if(byte0 == 7)
        {
            showHeartsOrSmokeFX(true);
        } else
        if(byte0 == 6)
        {
            showHeartsOrSmokeFX(false);
        } else
        if(byte0 == 8)
        {
        	isDogBusyShaking = true;
            timeDogIsShaking = 0.0F;
            prevTimeDogIsShaking = 0.0F;
        } else
        {
            super.handleHealthUpdate(byte0);
        }
    }

    public float setTailRotation()
    {
        if(isAngry())
        {
            return 1.53938F;
        }
        if(isTamed())
        {
            return (0.55F - (float)(maxHealth - dataWatcher.getWatchableObjectInt(18)) * 0.02F) * 3.141593F + (isSitting() ? 1 : 0);
        } else
        {
            return 0.6283185F;
        }
    }

    public int getMaxSpawnedInChunk()
    {
        return 4;
    }
    
    /**
     * gets this wolf's angry state
     */
    public boolean isAngry()
    {
        return (dataWatcher.getWatchableObjectByte(16) & 2) != 0;
    }

    /**
     * sets this wolf's angry state to true if the boolean argument is true
     */
    public void setAngry(boolean par1)
    {
        byte byte0 = dataWatcher.getWatchableObjectByte(16);

        if (par1)
        {
            dataWatcher.updateObject(16, Byte.valueOf((byte)(byte0 | 2)));
        }
        else
        {
            dataWatcher.updateObject(16, Byte.valueOf((byte)(byte0 & -3)));
        }
    }

	public EntityAnimal spawnBabyAnimal(EntityAnimal entityanimal) {
		return null;
	}

	public int getMaxHealth() {
		return maxHealth;
	}
	
	public boolean getCanSpawnHere()
    {
		
        int i = MathHelper.floor_double(posX);
        int j = MathHelper.floor_double(boundingBox.minY);
        int k = MathHelper.floor_double(posZ);
        System.out.println(worldObj.getBlockId(i, j - 2, k));
        return worldObj.getBlockId(i, j - 2, k) == Block.grass.blockID && worldObj.getFullBlockLightValue(i, j, k) > 8 && super.getCanSpawnHere();
    }
}
