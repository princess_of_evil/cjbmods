package net.minecraft.src;

import java.util.ArrayList;
import org.lwjgl.opengl.GL11;

public class CJB_BlockMesh extends Block
{

    protected CJB_BlockMesh(int i, Material material)
    {
        super(i, material);
        blockIndexInTexture = Block.blockSteel.blockIndexInTexture;
        float f = 0.1875F;
        setBlockBounds(0.0F, 0.8F - f / 2.0F, 0.0F, 1.0F, 0.8F + f / 2.0F, 1.0F);
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public int getRenderType()
    {
        return mod_cjb_items.MeshModelID;
    }
    
    /**
     * Triggered whenever an entity collides with this block (enters into the block). Args: world, x, y, z, entity
     */
    public void onEntityCollidedWithBlock(World world, int i, int j, int k, Entity entity)
    {
    	if (entity instanceof EntityItem) {
    		entity.setPosition(entity.posX, entity.posY - 0.005d, entity.posZ);
    	}
    }
    
    public void getCollidingBoundingBoxes(World world, int i, int j, int k, AxisAlignedBB axisalignedbb, ArrayList arraylist)
    {
    	setBlockBounds(0.0F, 0.8F, 0.0F, 1F, 0.9F, 1F);
        super.getCollidingBoundingBoxes(world, i, j, k, axisalignedbb, arraylist);
    }
    
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
    {
    	setBlockBounds(0.0F, 0.5F, 0.0F, 1F, 1.0F, 1F);
        return super.getSelectedBoundingBoxFromPool(world, i, j, k);
    }
    
    public void RenderInInv(RenderBlocks renderblocks, Block block, int i)
    {
        Tessellator tessellator = Tessellator.instance;
        for(int j = 0; j < 4; j++)
        {
            if(j == 0)
            {
            	block.setBlockBounds(0.0F, 0.8F, 0.2F, 1F, 0.9F, 0.3F);
            }
            if(j == 1)
            {
            	block.setBlockBounds(0.0F, 0.8F, 0.7F, 1F, 0.9F, 0.8F);
            } 
            if(j == 2)
            {
            	block.setBlockBounds(0.2F, 0.8F, 0.0F, 0.3F, 0.9F, 1F);
            }
            if(j == 3)
            {
            	block.setBlockBounds(0.7F, 0.8F, 0.0F, 0.8F, 0.9F, 1F);
            }
            GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
            GL11.glScalef(1f, 1f, 1f);
            tessellator.startDrawingQuads();
            tessellator.setNormal(0.0F, -1F, 0.0F);
            renderblocks.renderBottomFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(0, i));
            tessellator.draw();
            tessellator.startDrawingQuads();
            tessellator.setNormal(0.0F, 1.0F, 0.0F);
            renderblocks.renderTopFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(1, i));
            tessellator.draw();
            tessellator.startDrawingQuads();
            tessellator.setNormal(0.0F, 0.0F, -1F);
            renderblocks.renderEastFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(2, i));
            tessellator.draw();
            tessellator.startDrawingQuads();
            tessellator.setNormal(0.0F, 0.0F, 1.0F);
            renderblocks.renderWestFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(3, i));
            tessellator.draw();
            tessellator.startDrawingQuads();
            tessellator.setNormal(-1F, 0.0F, 0.0F);
            renderblocks.renderNorthFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(4, i));
            tessellator.draw();
            tessellator.startDrawingQuads();
            tessellator.setNormal(1.0F, 0.0F, 0.0F);
            renderblocks.renderSouthFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(5, i));
            tessellator.draw();
            GL11.glTranslatef(0.5F, 0.5F, 0.5F);
        }
        block.setBlockBounds(0.0F, 0.5F, 0.0F, 1F, 1.0F, 1F);
    }
    
    public boolean RenderInWorld(RenderBlocks renderblocks, IBlockAccess iblockaccess, int i, int j, int k, Block block)
    {
       
        block.setBlockBounds(0.0F, 0.8F, 0.2F, 1F, 0.9F, 0.3F);
        renderblocks.renderStandardBlock(block, i, j, k);
        
        block.setBlockBounds(0.0F, 0.8F, 0.7F, 1F, 0.9F, 0.8F);
        renderblocks.renderStandardBlock(block, i, j, k);
        
        block.setBlockBounds(0.2F, 0.8F, 0.0F, 0.3F, 0.9F, 1F);
        renderblocks.renderStandardBlock(block, i, j, k);
        
        block.setBlockBounds(0.7F, 0.8F, 0.0F, 0.8F, 0.9F, 1F);
        renderblocks.renderStandardBlock(block, i, j, k);
        return false;
    }
}
