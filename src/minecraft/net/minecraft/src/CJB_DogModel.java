// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package net.minecraft.src;

import org.lwjgl.opengl.GL11;

// Referenced classes of package net.minecraft.src:
//            ModelBase, ModelRenderer, Entitydog, MathHelper, 
//            EntityLiving

public class CJB_DogModel extends ModelBase
{
    public CJB_DogModel(float f)
    {
        float f1 = 13.5F;
        dogHeadMain = new ModelRenderer(this, 0, 0);
        dogHeadMain.addBox(-3F, -3F, -2F, 6, 6, 4, f);
        dogHeadMain.setRotationPoint(-1F, f1, -7F);
        dogBody = new ModelRenderer(this, 18, 14);
        dogBody.addBox(-4F, -2F, -3F, 6, 9, 6, f);
        dogBody.setRotationPoint(0.0F, 14F, 2.0F);
        dogMane = new ModelRenderer(this, 21, 0);
        dogMane.addBox(-3F, -3F, -3F, 6, 6, 6, f);
        dogMane.setRotationPoint(-1F, 14F, 2.0F);
        
        dogCollar = new ModelRenderer(this, 38, 25);
        dogCollar.addBox(-3.5F, -3F, -2.5F, 7, 1, 6, f);
        dogCollar.setRotationPoint(-1F, 14F, 2.0F);
        
        dogLeg1 = new ModelRenderer(this, 0, 18);
        dogLeg1.addBox(-1F, 0.0F, -1F, 2, 8, 2, f);
        dogLeg1.setRotationPoint(-2.5F, 16F, 7F);
        dogLeg2 = new ModelRenderer(this, 0, 18);
        dogLeg2.addBox(-1F, 0.0F, -1F, 2, 8, 2, f);
        dogLeg2.setRotationPoint(0.5F, 16F, 7F);
        dogLeg3 = new ModelRenderer(this, 0, 18);
        dogLeg3.addBox(-1F, 0.0F, -1F, 2, 8, 2, f);
        dogLeg3.setRotationPoint(-2.5F, 16F, -4F);
        dogLeg4 = new ModelRenderer(this, 0, 18);
        dogLeg4.addBox(-1F, 0.0F, -1F, 2, 8, 2, f);
        dogLeg4.setRotationPoint(0.5F, 16F, -4F);
        dogTail = new ModelRenderer(this, 9, 18);
        dogTail.addBox(-1F, 0.0F, -1F, 2, 8, 2, f);
        dogTail.setRotationPoint(-1F, 12F, 8F);
        dogRightEar = new ModelRenderer(this, 16, 14);
        dogRightEar.addBox(-3F, -5F, 0.0F, 2, 2, 1, f);
        dogRightEar.setRotationPoint(-1F, f1, -7F);
        dogLeftEar = new ModelRenderer(this, 16, 14);
        dogLeftEar.addBox(1.0F, -5F, 0.0F, 2, 2, 1, f);
        dogLeftEar.setRotationPoint(-1F, f1, -7F);
        
        dogRightEarTop = new ModelRenderer(this, 22, 14);
        dogRightEarTop.addBox(-3F, -5F, -1.0F, 2, 1, 1, f);
        dogRightEarTop.setRotationPoint(-1F, f1, -7F);
        dogLeftEarTop = new ModelRenderer(this, 22, 14);
        dogLeftEarTop.addBox(1.0F, -5F, -1.0F, 2, 1, 1, f);
        dogLeftEarTop.setRotationPoint(-1F, f1, -7F);
        
        dogSnout = new ModelRenderer(this, 0, 10);
        dogSnout.addBox(-2F, 0.0F, -5F, 3, 3, 4, f);
        dogSnout.setRotationPoint(-0.5F, f1, -7F);
        dogNose = new ModelRenderer(this, 43, 0);
        dogNose.addBox(-1.0F, -1.0F, -5F, 1, 1, 1, f);
        dogNose.setRotationPoint(-0.5F, f1, -7F);
        
        color = 0;
        tamed = false;
    }

    public void render(Entity enity, float f, float f1, float f2, float f3, float f4, float f5)
    {
        super.render(enity, f, f1, f2, f3, f4, f5);
        setRotationAngles(f, f1, f2, f3, f4, f5);
        
        dogHeadMain.renderWithRotation(f5);
        dogBody.render(f5);
        dogLeg1.render(f5);
        dogLeg2.render(f5);
        dogLeg3.render(f5);
        dogLeg4.render(f5);
        
        dogSnout.renderWithRotation(f5);
        dogNose.renderWithRotation(f5);
        dogTail.renderWithRotation(f5);
        dogMane.render(f5);
        
        dogRightEar.renderWithRotation(f5);
        dogLeftEar.renderWithRotation(f5);
        
        dogRightEarTop.renderWithRotation(f5);
        dogLeftEarTop.renderWithRotation(f5);
        
        dogCollar.showModel = tamed;
        
        float bright = 1f;
        GL11.glColor3f(bright * CJB_DogEntity.CollarColorTable[color][0], bright * CJB_DogEntity.CollarColorTable[color][1], bright * CJB_DogEntity.CollarColorTable[color][2]);
        dogCollar.render(f5);
        GL11.glColor3f(brightness, brightness, brightness);
    }
    
    public void setLivingAnimations(EntityLiving entityliving, float f, float f1, float f2)
    {
        CJB_DogEntity entitydog = (CJB_DogEntity)entityliving;
        if(entitydog.isAngry())
        {
            dogTail.rotateAngleY = 0.0F;
        } else
        {
            dogTail.rotateAngleY = MathHelper.cos(f * 0.6662F) * 1.4F * f1;
        }
        if(entitydog.isSitting())
        {
            dogMane.setRotationPoint(-1F, 16F, -3F);
            dogMane.rotateAngleX = 1.256637F;
            dogMane.rotateAngleY = 0.0F;
            dogCollar.setRotationPoint(-1F, 16F, -3F);
            dogCollar.rotateAngleX = 1.256637F;
            dogCollar.rotateAngleY = 0.0F;
            dogBody.setRotationPoint(0.0F, 18F, 0.0F);
            dogBody.rotateAngleX = 0.7853982F;
            dogTail.setRotationPoint(-1F, 21F, 6F);
            dogLeg1.setRotationPoint(-2.5F, 22F, 2.0F);
            dogLeg1.rotateAngleX = 4.712389F;
            dogLeg2.setRotationPoint(0.5F, 22F, 2.0F);
            dogLeg2.rotateAngleX = 4.712389F;
            dogLeg3.rotateAngleX = 5.811947F;
            dogLeg3.setRotationPoint(-2.49F, 17F, -4F);
            dogLeg4.rotateAngleX = 5.811947F;
            dogLeg4.setRotationPoint(0.51F, 17F, -4F);
        } else
        {
            dogBody.setRotationPoint(0.0F, 14F, 2.0F);
            dogBody.rotateAngleX = 1.570796F;
            dogMane.setRotationPoint(-1F, 14F, -3F);
            dogMane.rotateAngleX = dogBody.rotateAngleX;
            dogCollar.setRotationPoint(-1F, 14F, -3F);
            dogCollar.rotateAngleX = dogBody.rotateAngleX;
            dogTail.setRotationPoint(-1F, 12F, 8F);
            dogLeg1.setRotationPoint(-2.5F, 16F, 7F);
            dogLeg2.setRotationPoint(0.5F, 16F, 7F);
            dogLeg3.setRotationPoint(-2.5F, 16F, -4F);
            dogLeg4.setRotationPoint(0.5F, 16F, -4F);
            dogLeg1.rotateAngleX = MathHelper.cos(f * 0.6662F) * 1.4F * f1;
            dogLeg2.rotateAngleX = MathHelper.cos(f * 0.6662F + 3.141593F) * 1.4F * f1;
            dogLeg3.rotateAngleX = MathHelper.cos(f * 0.6662F + 3.141593F) * 1.4F * f1;
            dogLeg4.rotateAngleX = MathHelper.cos(f * 0.6662F) * 1.4F * f1;
        }
        float f3 = entitydog.getInterestedAngle(f2) + entitydog.getShakeAngle(f2, 0.0F);
        dogHeadMain.rotateAngleZ = f3;
        dogRightEar.rotateAngleZ = f3;
        dogLeftEar.rotateAngleZ = f3;
        dogRightEarTop.rotateAngleZ = f3;
        dogLeftEarTop.rotateAngleZ = f3;
        dogSnout.rotateAngleZ = f3;
        dogNose.rotateAngleZ = f3;
        dogMane.rotateAngleZ = entitydog.getShakeAngle(f2, -0.08F);
        dogCollar.rotateAngleZ = entitydog.getShakeAngle(f2, -0.08F);
        dogBody.rotateAngleZ = entitydog.getShakeAngle(f2, -0.16F);
        dogTail.rotateAngleZ = entitydog.getShakeAngle(f2, -0.2F);
        
        brightness = entitydog.getBrightnessForRender(f2);
        if(entitydog.isDogShaking)
        {
            float f4 = brightness * entitydog.getShadingWhileShaking(f2);
            GL11.glColor3f(f4, f4, f4);
        }
        
        color = entitydog.collarColor;
        tamed = entitydog.isTamed();
        
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5)
    {
        super.setRotationAngles(f, f1, f2, f3, f4, f5);
        dogHeadMain.rotateAngleX = f4 / 57.29578F;
        dogHeadMain.rotateAngleY = f3 / 57.29578F;
        dogRightEar.rotateAngleY = dogHeadMain.rotateAngleY;
        dogRightEar.rotateAngleX = dogHeadMain.rotateAngleX;
        dogLeftEar.rotateAngleY = dogHeadMain.rotateAngleY;
        dogLeftEar.rotateAngleX = dogHeadMain.rotateAngleX;
        
        dogRightEarTop.rotateAngleY = dogHeadMain.rotateAngleY;
        dogRightEarTop.rotateAngleX = dogHeadMain.rotateAngleX;
        dogLeftEarTop.rotateAngleY = dogHeadMain.rotateAngleY;
        dogLeftEarTop.rotateAngleX = dogHeadMain.rotateAngleX;
        
        dogSnout.rotateAngleY = dogHeadMain.rotateAngleY;
        dogSnout.rotateAngleX = dogHeadMain.rotateAngleX;
        dogNose.rotateAngleY = dogHeadMain.rotateAngleY;
        dogNose.rotateAngleX = dogHeadMain.rotateAngleX;
        dogTail.rotateAngleX = f2;
    }

    public ModelRenderer dogHeadMain;
    public ModelRenderer dogBody;
    public ModelRenderer dogLeg1;
    public ModelRenderer dogLeg2;
    public ModelRenderer dogLeg3;
    public ModelRenderer dogLeg4;
    public ModelRenderer dogRightEar;
    public ModelRenderer dogLeftEar;
    public ModelRenderer dogRightEarTop;
    public ModelRenderer dogLeftEarTop;
    public ModelRenderer dogSnout;
    public ModelRenderer dogNose;
    public ModelRenderer dogTail;
    public ModelRenderer dogMane;
    public ModelRenderer dogCollar;
    int color;
    boolean tamed;
    float brightness;
}
