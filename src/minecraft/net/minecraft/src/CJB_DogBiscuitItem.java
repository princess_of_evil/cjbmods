package net.minecraft.src;

public class CJB_DogBiscuitItem extends Item
{

    public CJB_DogBiscuitItem(int i)
    {
        super(i);
        maxStackSize = 64;
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        itemstack.stackSize--;
        world.playSoundAtEntity(entityplayer, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));
        if(!world.isRemote)
        {
            world.spawnEntityInWorld(new CJB_DogBiscuitEntity(world, entityplayer));
        }
        return itemstack;
    }
}
