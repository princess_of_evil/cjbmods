package net.minecraft.src;

public class CJB_PWBGuiCrafting extends GuiCrafting
{
    public CJB_PWBGuiCrafting(InventoryPlayer inventoryplayer, World world)
    {
    	super(inventoryplayer, world, 0, 0, 0);
    	inventorySlots = new CJB_PWBContainer(inventoryplayer, world, 0, 0, 0);
    }
    
    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Portable Workbench", 8, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }
}
