package net.minecraft.src;

public class CJB_DogInventory
    implements IInventory
{

    public CJB_DogInventory(CJB_DogEntity entitydog)
    {
    	inventory = new ItemStack[9];
    	armorInventory = new ItemStack[1];
        inventoryChanged = false;
        dog = entitydog;
    }

    private int getInventorySlotContainItem(int i)
    {
        for(int j = 0; j < inventory.length; j++)
        {
            if(inventory[j] != null && inventory[j].itemID == i)
            {
                return j;
            }
        }

        return -1;
    }

    public int storeItemStack(ItemStack itemstack)
    {
        for(int i = 0; i < inventory.length; i++)
        {
            if(inventory[i] != null && inventory[i].itemID == itemstack.itemID && inventory[i].isStackable() && inventory[i].stackSize < inventory[i].getMaxStackSize() && inventory[i].stackSize < getInventoryStackLimit() && (!inventory[i].getHasSubtypes() || inventory[i].getItemDamage() == itemstack.getItemDamage()))
            {
                return i;
            }
        }

        return -1;
    }

    public int getFirstEmptyStack()
    {
        for(int i = 0; i < inventory.length; i++)
        {
            if(inventory[i] == null)
            {
                return i;
            }
        }

        return -1;
    }

    private int storePartialItemStack(ItemStack itemstack)
    {
        int i = itemstack.itemID;
        int j = itemstack.stackSize;
        int k = storeItemStack(itemstack);
        if(k < 0)
        {
            k = getFirstEmptyStack();
        }
        if(k < 0)
        {
            return j;
        }
        if(inventory[k] == null)
        {
        	inventory[k] = new ItemStack(i, 0, itemstack.getItemDamage());
        }
        int l = j;
        if(l > inventory[k].getMaxStackSize() - inventory[k].stackSize)
        {
            l = inventory[k].getMaxStackSize() - inventory[k].stackSize;
        }
        if(l > getInventoryStackLimit() - inventory[k].stackSize)
        {
            l = getInventoryStackLimit() - inventory[k].stackSize;
        }
        if(l == 0)
        {
            return j;
        } else
        {
            j -= l;
            inventory[k].stackSize += l;
            inventory[k].animationsToGo = 5;
            return j;
        }
    }

    public boolean consumeInventoryItem(int i)
    {
        int j = getInventorySlotContainItem(i);
        if(j < 0)
        {
            return false;
        }
        if(--inventory[j].stackSize <= 0)
        {
        	inventory[j] = null;
        }
        return true;
    }

    public boolean addItemStackToInventory(ItemStack itemstack)
    {
        if(!itemstack.isItemDamaged())
        {
            int i;
            do
            {
                i = itemstack.stackSize;
                itemstack.stackSize = storePartialItemStack(itemstack);
            } while(itemstack.stackSize > 0 && itemstack.stackSize < i);
            return itemstack.stackSize < i;
        }
        int j = getFirstEmptyStack();
        if(j >= 0)
        {
        	inventory[j] = ItemStack.copyItemStack(itemstack);
            itemstack.stackSize = 0;
            return true;
        } else
        {
            return false;
        }
    }

    public ItemStack decrStackSize(int i, int j)
    {
        ItemStack aitemstack[] = inventory;
        if(i >= inventory.length)
        {
            aitemstack = armorInventory;
            i -= inventory.length;
        }
        if(aitemstack[i] != null)
        {
            if(aitemstack[i].stackSize <= j)
            {
                ItemStack itemstack = aitemstack[i];
                aitemstack[i] = null;
                return itemstack;
            }
            ItemStack itemstack1 = aitemstack[i].splitStack(j);
            if(aitemstack[i].stackSize == 0)
            {
                aitemstack[i] = null;
            }
            return itemstack1;
        } else
        {
            return null;
        }
    }

    public void setInventorySlotContents(int i, ItemStack itemstack)
    {
        ItemStack aitemstack[] = inventory;
        if(i >= aitemstack.length)
        {
            i -= aitemstack.length;
            aitemstack = armorInventory;
        }
        aitemstack[i] = itemstack;
    }

    public NBTTagList writeToNBT(NBTTagList nbttaglist)
    {
        for(int i = 0; i < inventory.length; i++)
        {
            if(inventory[i] != null)
            {
                NBTTagCompound nbttagcompound = new NBTTagCompound();
                nbttagcompound.setByte("Slot", (byte)i);
                inventory[i].writeToNBT(nbttagcompound);
                nbttaglist.appendTag(nbttagcompound);
            }
        }
        for(int j = 0; j < armorInventory.length; j++)
        {
            if(armorInventory[j] != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)(j + 100));
                armorInventory[j].writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }
        return nbttaglist;
    }

    public void readFromNBT(NBTTagList nbttaglist)
    {
    	inventory = new ItemStack[9];
    	armorInventory = new ItemStack[1];
        for(int i = 0; i < nbttaglist.tagCount(); i++)
        {
            NBTTagCompound nbttagcompound = (NBTTagCompound)nbttaglist.tagAt(i);
            int j = nbttagcompound.getByte("Slot") & 0xff;
            ItemStack itemstack = ItemStack.loadItemStackFromNBT(nbttagcompound);
            if(itemstack.getItem() == null)
            {
                continue;
            }
            if(j >= 0 && j < inventory.length)
            {
            	inventory[j] = itemstack;
            }
            if(j >= 100 && j < armorInventory.length + 100)
            {
                armorInventory[j - 100] = itemstack;
            }
        }

    }

    public int getSizeInventory()
    {
        return inventory.length + 1;
    }
    
    public ItemStack getStackInSlot(int i)
    {
        ItemStack aitemstack[] = inventory;
        if(i >= aitemstack.length)
        {
            i -= aitemstack.length;
            aitemstack = armorInventory;
        }
        return aitemstack[i];
    }
    
    public String getInvName()
    {
        return "Dog Inventory";
    }

    public int getInventoryStackLimit()
    {
        return 64;
    }

    public void onInventoryChanged()
    {
        inventoryChanged = true;
    }

    public boolean isUseableByPlayer(EntityPlayer entityplayer)
    {
        if(dog.isDead)
        {
            return false;
        }
        return entityplayer.getDistanceSqToEntity(dog) <= 64D;
    }
    
    public ItemStack armorItemInSlot(int i)
    {
        return armorInventory[i];
    }
    
    public int getTotalArmorValue()
    {
        for(int l = 0; l < armorInventory.length; l++)
        {
            if(armorInventory[l] != null && (armorInventory[l].getItem() instanceof ItemArmor))
            {
                return 4 + (((ItemArmor)armorInventory[l].getItem()).getMaxDamage()/24);
            }
        }

        return 0;
    }

    public boolean func_28018_c(ItemStack itemstack)
    {
    	
    	for(int i = 0; i < armorInventory.length; i++)
        {
            if(armorInventory[i] != null && armorInventory[i].isStackEqual(itemstack))
            {
                return true;
            }
        }
    	
        for(int j = 0; j < inventory.length; j++)
        {
            if(inventory[j] != null && inventory[j].isStackEqual(itemstack))
            {
                return true;
            }
        }

        return false;
    }
    
    public void dropAllItems()
    {
        for(int i = 0; i < inventory.length; i++)
        {
            if(inventory[i] != null)
            {
            	dog.entityDropItem(inventory[i], 0);
            	inventory[i] = null;
            }
        }
        
        for(int j = 0; j < armorInventory.length; j++)
        {
            if(armorInventory[j] != null)
            {
            	dog.entityDropItem(armorInventory[j], 0);
                armorInventory[j] = null;
            }
        }

    }

    public ItemStack inventory[];
    public ItemStack armorInventory[];
    public CJB_DogEntity dog;
    public boolean inventoryChanged;


	@Override
	public void openChest() {
		
	}

	@Override
	public void closeChest() {
		
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		return null;
	}
}
